﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WishList2.Business.Models;
using WishList2.Business.Interactors.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace wishlist2.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class UsersController : ControllerBase
  {
    private readonly IUserInteractor _userInteractor;

    public UsersController(IUserInteractor userInteractor)
    {
      _userInteractor = userInteractor;
    }

    [HttpGet]
    public async Task<IEnumerable<User>> Get()
    {
      return await _userInteractor.GetUsers();
    }
  }
}
