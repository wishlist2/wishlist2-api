﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WishList2.Business.Models;
using WishList2.Business.Interactors.Interfaces;
using System;

namespace wishlist2.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class ItemsController : ControllerBase
  {
    private readonly IItemInteractor _itemInteractor;

    public ItemsController(IItemInteractor itemsInteractor)
    {
      _itemInteractor = itemsInteractor;
    }

    [HttpGet]
    public async Task<IEnumerable<Item>> Get(Guid userId)
    {
      return await _itemInteractor.GetItemsByUserId(userId);
    }
  }
}
