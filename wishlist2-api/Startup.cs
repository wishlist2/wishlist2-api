using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WishList2.Business;
using WishList2.Business.Interactors;
using WishList2.Business.Interactors.Interfaces;
using WishList2.Data.Repositories;
using WishList2.Data.Repositories.Interfaces;

namespace MyAspNetCoreWebApi
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddControllers();
      services.AddScoped<IDbConnection>(p => new SqlConnection(Configuration.GetConnectionString("wishlist2-db")));
      services.AddTransient<IUserRepository, UserRepository>();
      services.AddTransient<IItemRepository, ItemRepository>();
      services.AddTransient<IUserInteractor, UserInteractor>();
      services.AddTransient<IItemInteractor, ItemInteractor>();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseRouting();

      app.UseAuthorization();

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });
    }
  }
}
