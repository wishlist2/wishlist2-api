﻿using System;

namespace WishList2.Data.Models
{
  public class ItemDetail
  {
    public Guid ItemId { get; set; }
    public string Description { get; set; }
    public DateTime DateModified { get; set; }
  }
}
