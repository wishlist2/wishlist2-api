﻿using System;

namespace WishList2.Data.Models
{
  public class User
  {
    public Guid Id { get; set; }
    public string UserId { get; set; }
    public string Email { get; set; }
    public string Name { get; set; }
    public bool IsApproved { get; set; }
    public DateTime? LastLogin { get; set; }
  }
}
