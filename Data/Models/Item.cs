﻿using System;
using System.Collections.Generic;

namespace WishList2.Data.Models
{
  public class Item
  {
    public Guid Id { get; set; }
    public Guid UserId { get; set; }
    public string NewestDescription { get; set; }
    public double SortOrder { get; set; }
    public DateTime DateAdded { get; set; }

    public IList<ItemDetail> Details { get; set; }

    public Item()
    {
      Details = new List<ItemDetail>();
    }
  }
}
