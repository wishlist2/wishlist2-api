﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WishList2.Data.Models;
using WishList2.Data.Repositories.Interfaces;

namespace WishList2.Data.Repositories
{
    public class ItemRepository : IItemRepository
    {
        private readonly IDbConnection _connection;

        public ItemRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public async Task<IEnumerable<Item>> GetItemsByUserId(Guid userId)
        {
            var items = new Dictionary<Guid, Item>();
            return (
                await _connection.QueryAsync<Item, ItemDetail, Item>(@"
                    SELECT Id, UserId, NewestDescription, SortOrder, DateAdded, ItemId, Description, DateModified
                        FROM vwItems
                        WHERE UserId = @userId
                    ",
                    (item, detail) =>
                    {
                        Item existingItem;
                        if (false == items.TryGetValue(item.Id, out existingItem))
                        {
                            existingItem = item;
                            items.Add(item.Id, item);
                        }
                        if (detail != null)
                        {
                            existingItem.Details.Add(detail);
                        }
                        return existingItem;
                    },
                    new { userId },
                    splitOn: "ItemId"
                )
            )
            .Distinct()
            .ToList();
        }
    }
}
