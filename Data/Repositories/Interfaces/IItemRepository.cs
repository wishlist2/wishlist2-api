﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WishList2.Data.Models;

namespace WishList2.Data.Repositories.Interfaces
{
  public interface IItemRepository
  {
    Task<IEnumerable<Item>> GetItemsByUserId(Guid userId);
  }
}