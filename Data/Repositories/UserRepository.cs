﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using WishList2.Data.Models;
using WishList2.Data.Repositories.Interfaces;

namespace WishList2.Data.Repositories
{
  public class UserRepository : IUserRepository
  {
    private readonly IDbConnection _connection;

    public UserRepository(IDbConnection connection)
    {
      _connection = connection;
    }

    public Task<IEnumerable<User>> GetUsers()
    {
      return _connection.QueryAsync<User>(@"
        SELECT [u].[Id], [u].[UserId], [u].[Email], [u].[Name], [u].[IsApproved], [u].[LastLogin]
          FROM [Users] [u]
          ORDER BY [u].[Name]
        "
      );
    }
  }
}
