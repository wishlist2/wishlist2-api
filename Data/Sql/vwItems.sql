﻿IF EXISTS (SELECT * FROM sys.views  WHERE object_id = OBJECT_ID(N'dbo.vwItems'))
BEGIN
    DROP VIEW dbo.vwItems
END
GO

CREATE VIEW dbo.vwItems
AS
	SELECT i.Id, i.UserId,
		(
			SELECT TOP 1 Description
				FROM ItemDetails
				WHERE ItemId = i.Id
				ORDER BY DateModified DESC
		) AS NewestDescription,
		i.SortOrder, i.DateAdded, id.ItemId, id.Description, id.DateModified
		FROM Items i
		LEFT OUTER JOIN ItemDetails id ON i.Id = id.ItemId
GO
