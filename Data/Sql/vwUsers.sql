﻿IF EXISTS (SELECT * FROM sys.views  WHERE object_id = OBJECT_ID(N'[dbo].[vwUsers]'))
BEGIN
    DROP VIEW [dbo].[vwUsers]
END
GO

CREATE VIEW [dbo].[vwUsers]
AS
	SELECT [u].Id, [u].[UserId], [u].[Email], [u].[Name], [u].[IsApproved], [m].[CanManageUserId], [mu].[Name] AS [CanManagerUserName]
		FROM [dbo].[Users] [u]
		LEFT OUTER JOIN [dbo].[UserManagement] [m] ON [u].[Id] = [m].[UserId]
		LEFT OUTER JOIN [dbo].[Users] [mu] ON [m].[CanManageUserId] = [mu].[Id]
GO
