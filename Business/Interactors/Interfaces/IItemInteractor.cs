﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WishList2.Business.Models;

namespace WishList2.Business.Interactors.Interfaces
{
  public interface IItemInteractor
  {
    Task<IEnumerable<Item>> GetItemsByUserId(Guid userId);
  }
}