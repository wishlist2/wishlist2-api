﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WishList2.Business.Interactors.Interfaces;
using WishList2.Business.Models;
using WishList2.Data.Repositories.Interfaces;

namespace WishList2.Business.Interactors
{
  public class UserInteractor : IUserInteractor
  {
    private readonly IUserRepository _userRepository;

    public UserInteractor(IUserRepository userRepository)
    {
      _userRepository = userRepository;
    }

    public async Task<IEnumerable<User>> GetUsers()
    {
      var users = await _userRepository.GetUsers();

      return users.Select(u => new User()
        {
          Id = u.Id,
          UserId = u.UserId,
          Email = u.Email,
          Name = u.Name,
          IsApproved = u.IsApproved,
          LastLogin = u.LastLogin
        }
      );
    }
  }
}
