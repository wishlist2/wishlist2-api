﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WishList2.Business.Models;
using WishList2.Business.Interactors.Interfaces;
using WishList2.Data.Repositories.Interfaces;

namespace WishList2.Data.Repositories
{
    public class ItemInteractor : IItemInteractor
    {
        private readonly IItemRepository _itemRepository;

        public ItemInteractor(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task<IEnumerable<Item>> GetItemsByUserId(Guid userId)
        {
            return (await _itemRepository.GetItemsByUserId(userId))
                .Select(i => new Item()
                    {
                        Id = i.Id,
                        UserId = i.UserId,
                        SortOrder = i.SortOrder,
                        NewestDescription = i.NewestDescription,
                        DateAdded = i.DateAdded,
                        Details = i.Details.Select(d => new ItemDetail()
                            {
                                ItemId = d.ItemId,
                                Description = d.Description,
                                DateModified = d.DateModified
                            }
                        ).ToList()
                    }
                );
        }
    }
}
